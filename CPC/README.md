# Faceted Frequency Browser

This is the main directory for Faceted Frequency Browser's source code. 

FFB was formerly called 'Conditional Probability Calculator' (CPC). 
For consistency, the tool is referred by this original name in this directory. A refactoring could be done once the name FFB is no longer a working title, but is final.

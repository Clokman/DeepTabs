# DeepTabs 
This repository contains a prototype interactive visualization that helps explore nested datasets.

The latest prototype can be interacted with at:
[https://clokman.github.io/TEPAIV/CPC/](https://clokman.github.io/TEPAIV/CPC/) .
Please note that the prototype at this link is a proof of concept, and it may contain some bugs.